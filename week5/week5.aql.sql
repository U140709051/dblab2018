select *
from movies;

# 1. show the flims whose budet is greater than 10 million$ and ranking is less than 6.
select title
from movies
where budget > 10000000 and ranking < 6;
# 2. show the action films whose rating is greater than 8.8 and produced before 2009.
select title
from movies 
where rating > 8.8 and year < 2009 and movie_id in(
	select movie_id 
    from genres
    where genre_name = "Action"
);

# 3. show the drama films whose duration is more than 150 minutes and oscars is more than 2.
select title
from movies 
where duration>150 and oscars>2 and movie_id in (
	select movie_id
    from genres
    where genre_name = "Drama"
);
#4. show the films that orlando bloom and Ian mckellen have act together and have more than 2 oscars;
select title 
from movies 
where oscars>2 and movie_id in (
	select movie_id
    from stars join movie_stars on stars.star_id = movie_stars.star_id
    where star_name = "Orlando bloom"  and movie_id in (
		select movie_id
		from stars join movie_stars on stars.star_id = movie_stars.star_id
		where star_name = "Ian Mckllen")
);