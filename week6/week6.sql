select *
from movies;

# 1. show the flims whose budet is greater than 10 million$ and ranking is less than 6.
select title
from movies
where budget > 10000000 and ranking < 6;
# 2. show the action films whose rating is greater than 8.8 and produced before 2009.
select title
from movies 
where rating > 8.8 and year < 2009 and movie_id in(
	select movie_id 
    from genres
    where genre_name = "Action"
);

# 3. show the drama films whose duration is more than 150 minutes and oscars is more than 2.
select title
from movies 
where duration>150 and oscars>2 and movie_id in (
	select movie_id
    from genres
    where genre_name = "Drama"
);
#4. show the films that orlando bloom and Ian mckellen have act together and have more than 2 oscars;
select title 
from movies 
where oscars>2 and movie_id in (
	select movie_id
    from stars join movie_stars on stars.star_id = movie_stars.star_id
    where star_name = "Orlando bloom"  and movie_id in (
		select movie_id
		from stars join movie_stars on stars.star_id = movie_stars.star_id
		where star_name = "Ian Mckllen")
);
#5. Show the Quentin Tarantino films which have more than 500000 votes and produced before 2000.
select *
from movies;

# 1. show the flims whose budet is greater than 10 million$ and ranking is less than 6.
select title
from movies
where budget > 10000000 and ranking < 6;
# 2. show the action films whose rating is greater than 8.8 and produced before 2009.
select title
from movies 
where rating > 8.8 and year < 2009 and movie_id in(
	select movie_id 
    from genres
    where genre_name = "Action"
);

# 3. show the drama films whose duration is more than 150 minutes and oscars is more than 2.
select title
from movies 
where duration>150 and oscars>2 and movie_id in (
	select movie_id
    from genres
    where genre_name = "Drama"
);
#4. show the films that orlando bloom and Ian mckellen have act together and have more than 2 oscars;
select title 
from movies 
where oscars>2 and movie_id in (
	select movie_id
    from stars join movie_stars on stars.star_id = movie_stars.star_id
    where star_name = "Orlando bloom"  and movie_id in (
		select movie_id
		from stars join movie_stars on stars.star_id = movie_stars.star_id
		where star_name = "Ian Mckllen")
);
#5. Show the Quentin Tarantino films which have more than 500000 votes and produced before 2000.
select movie_id,title
from movies
where votes>500000 and year<2000 and movie_id in(
	select movie_id
	from directors join movie_directors on directors.director_id = movie_directors.director_id 
	where director_name = "Quentin Tarantino");
    
# 6. Show the thriller films whose budget is greater than 25 million$.	 
select title
from movies join genres on movies.movie_id = genres.movie_id
where budget > 25000000 and genre_name = "Thriller";

# 7. Show the drama films whose language is Italian and produced between 1990-2000.
select title
from movies
where year  between 1990 and 2000 and movie_id in (
	select languages.movie_id
    from languages join genres on languages.movie_id = genres.movie_id
    where language_name = "italian" and genre_name = "Drama"
    );
    
# 8. Show the films that Tom Hanks has act and have won more than 3 Oscars.	
select title 
from movies
where oscars > 3 and movie_id in(
	select movie_id 
    from stars join movie_stars on stars.star_id = movie_stars.star_id
    where star_name like "tom hanks"
);

# 9. Show the history films produced in USA and whose duration is between 100-200 minutes.
select title
from movies join genres on movies.movie_id = movie_id
where duration between 100 and 200 and genre_name = "History" and .movie_id in (
	select movie_id
    from countries join producer_countries on countries.country_id = producer_countries
    where country_name like "usa"
    );
    
    
# 10.Compute the average budget of the films directed by Peter Jackson.
select title , budget
from movies
where movie_id in (
	select movie_id
    from directors join movie_directors on directors.director_id = movie_directors
    where director_name = "peter Jackson"
);
# 11.Show the Francis Ford Coppola film that has the minimum budget.
select  min(budget)
from movies
where movie_id in (
	select movie_id
    from directors join movie_directors on directors.director_id = movie_directors
    where director_name = "Francis Ford Coppola"
);
# 12.Show the film that has the most vote and has been produced in USA.
select title , votes
from movies 
where movie_id in (
	select movie_id 
    from countries join producer_countries on countries.country_id= producer_countries
    where country_name = "USA"
    )
order by voes desc
limit 1;